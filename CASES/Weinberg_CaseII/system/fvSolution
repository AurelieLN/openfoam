/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  4.0                                   |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    "alpha.*"
    {
        nAlphaCorr      2;
        nAlphaSubCycles 4;
        alphaOuterCorrectors yes;
        cAlpha          2;

        MULESCorr       yes;
        nLimiterIter    3;

        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-8;
        relTol          0;
    }


    pcorr
    {
        solver          PCG;
        preconditioner
        {
            preconditioner  GAMG;
            tolerance       1e-05;
            relTol          0;
            smoother        GaussSeidel;
           nPreSweeps      0; //
            nPostSweeps     2;//
            nFinestSweeps   2;//
            cacheAgglomeration on;//
            nCellsInCoarsestLevel 10;//
            agglomerator    faceAreaPair;//
            mergeLevels     1;//
        }
        tolerance       1e-05;
        relTol          0;
        maxIter         100;
    }

    p_rgh
    {
        solver          GAMG;
        tolerance       1e-07;
        relTol          0.05;
        smoother        GaussSeidel;
        nPreSweeps      0;//
        nPostSweeps     2;//
        nFinestSweeps   2;//
        cacheAgglomeration on;//
        nCellsInCoarsestLevel 10;//
        agglomerator    faceAreaPair;//
        mergeLevels     1;//
    }

    p_rghFinal
    {
        solver          PCG;
        preconditioner
        {
            preconditioner  GAMG;
            tolerance       1e-07;
            relTol          0;
            nVcycles        2;
            smoother        GaussSeidel;
            nPreSweeps      0;//
            nPostSweeps     2;
            nFinestSweeps   2;
            cacheAgglomeration on;
            nCellsInCoarsestLevel 10;
            agglomerator    faceAreaPair;
            mergeLevels     1;//
        }
        tolerance       1e-07;
        relTol          0;
        maxIter         20;
    }

    U
    {
        solver          smoothSolver;
        smoother        GaussSeidel;
        tolerance       1e-08;
        relTol          0.1;
        nSweeps         1;
    }

    UFinal
    {
        $U;
        tolerance       1e-08;
        relTol          0;
    }
}

PIMPLE
{
    momentumPredictor   no;
    nOuterCorrectors    1;
    nCorrectors         3;
    nNonOrthogonalCorrectors 0;
    pRefPoint       (0 0 0);
    pRefValue       0;
}

relaxationFactors
{
    equations
    {
        "U.*"           1;
    }
}

// ************************************************************************* //
