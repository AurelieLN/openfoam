We use OpenFOAM-4.0 and his two-layer solver *interFoam* to create a new
two-layer, temperature with Boussinesq approximation solver, called *interFoamT*.
We include setup to solve cases presented in the paper "3D modeling of crustal 
polydiapirs with VOF methods".

To give more details : in the paper "3D modeling of crustal polydiapirs with VOF
methods", we present benchmarks for 2 and 3 layers Rayleigh-Taylor cases which 
can be solved with *interFoam* and *multiphaseInterFoam* respectively (current 
version of OpenFOAM or anterior). Theses solvers are dedicated to solve, with a 
VOF method, problems with interface.
Moreover, in the paper, we present benchmarks of 1 and 2 layers 
Rayleigh-Bénard cases solved by *interFoamT*.
It deals with thermal two-layer problem. We add temperature equation and 
Boussinesq approximation to the solver InterFoam.
Finally, we include setup presented in the paper for both Rayleigh-Taylor and
Rayleigh-Bénard problems. There are various cases for each benchmark. 
For example, for the two-layer Rayleigh-Bénard system, we present 4 cases where
the density of fluid 2 is varied, values for modified parameters are in
commentary in the file *transportProperties*.