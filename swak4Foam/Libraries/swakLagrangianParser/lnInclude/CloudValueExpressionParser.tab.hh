// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton interface for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

/**
 ** \file CloudValueExpressionParser.tab.hh
 ** Define the parserCloud::parser class.
 */

// C++ LALR(1) parser skeleton written by Akim Demaille.

#ifndef YY_PARSERCLOUD_CLOUDVALUEEXPRESSIONPARSER_TAB_HH_INCLUDED
# define YY_PARSERCLOUD_CLOUDVALUEEXPRESSIONPARSER_TAB_HH_INCLUDED


# include <cstdlib> // std::abort
# include <iostream>
# include <stdexcept>
# include <string>
# include <vector>
# include "CloudValueExpressionParser_stack.hh"
# include "CloudValueExpressionParser_location.hh"


#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* Debug traces.  */
#ifndef PARSERCLOUDDEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PARSERCLOUDDEBUG 1
#  else
#   define PARSERCLOUDDEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PARSERCLOUDDEBUG 1
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PARSERCLOUDDEBUG */


namespace parserCloud {
#line 122 "CloudValueExpressionParser.tab.hh" // lalr1.cc:377





  /// A Bison parser.
  class CloudValueExpressionParser
  {
  public:
#ifndef PARSERCLOUDSTYPE
    /// Symbol semantic values.
    union semantic_type
    {
    #line 81 "../CloudValueExpressionParser.yy" // lalr1.cc:377

    Foam::scalar val;
    Foam::label integer;
    Foam::vector *vec;
    Foam::word *name;
    Foam::vectorField *vfield;
    Foam::scalarField *sfield;
    Foam::Field<bool> *lfield;
    Foam::tensorField *tfield;
    Foam::symmTensorField *yfield;
    Foam::sphericalTensorField *hfield;

#line 149 "CloudValueExpressionParser.tab.hh" // lalr1.cc:377
    };
#else
    typedef PARSERCLOUDSTYPE semantic_type;
#endif
    /// Symbol locations.
    typedef location location_type;

    /// Syntax errors thrown from user actions.
    struct syntax_error : std::runtime_error
    {
      syntax_error (const location_type& l, const std::string& m);
      location_type location;
    };

    /// Tokens.
    struct token
    {
      enum yytokentype
      {
        TOKEN_LINE = 258,
        TOKEN_LOOKUP = 259,
        TOKEN_LOOKUP2D = 260,
        TOKEN_SID = 261,
        TOKEN_VID = 262,
        TOKEN_LID = 263,
        TOKEN_TID = 264,
        TOKEN_YID = 265,
        TOKEN_HID = 266,
        TOKEN_GAS_SID = 267,
        TOKEN_GAS_VID = 268,
        TOKEN_GAS_TID = 269,
        TOKEN_GAS_YID = 270,
        TOKEN_GAS_HID = 271,
        TOKEN_FUNCTION_SID = 272,
        TOKEN_FUNCTION_VID = 273,
        TOKEN_FUNCTION_TID = 274,
        TOKEN_FUNCTION_YID = 275,
        TOKEN_FUNCTION_HID = 276,
        TOKEN_FUNCTION_LID = 277,
        TOKEN_SETID = 278,
        TOKEN_ZONEID = 279,
        TOKEN_NUM = 280,
        TOKEN_INT = 281,
        TOKEN_VEC = 282,
        START_DEFAULT = 289,
        START_CLOUD_SCALAR_COMMA = 290,
        START_CLOUD_SCALAR_CLOSE = 291,
        START_CLOUD_VECTOR_COMMA = 292,
        START_CLOUD_VECTOR_CLOSE = 293,
        START_CLOUD_TENSOR_COMMA = 294,
        START_CLOUD_TENSOR_CLOSE = 295,
        START_CLOUD_YTENSOR_COMMA = 296,
        START_CLOUD_YTENSOR_CLOSE = 297,
        START_CLOUD_HTENSOR_COMMA = 298,
        START_CLOUD_HTENSOR_CLOSE = 299,
        START_CLOUD_LOGICAL_COMMA = 300,
        START_CLOUD_LOGICAL_CLOSE = 301,
        START_CLOSE_ONLY = 302,
        START_COMMA_ONLY = 303,
        TOKEN_LAST_FUNCTION_CHAR = 304,
        TOKEN_IN_FUNCTION_CHAR = 305,
        TOKEN_VECTOR = 306,
        TOKEN_TENSOR = 307,
        TOKEN_SYMM_TENSOR = 308,
        TOKEN_SPHERICAL_TENSOR = 309,
        TOKEN_TRUE = 310,
        TOKEN_FALSE = 311,
        TOKEN_x = 312,
        TOKEN_y = 313,
        TOKEN_z = 314,
        TOKEN_xx = 315,
        TOKEN_xy = 316,
        TOKEN_xz = 317,
        TOKEN_yx = 318,
        TOKEN_yy = 319,
        TOKEN_yz = 320,
        TOKEN_zx = 321,
        TOKEN_zy = 322,
        TOKEN_zz = 323,
        TOKEN_ii = 324,
        TOKEN_fluidPhase = 325,
        TOKEN_unitTensor = 326,
        TOKEN_pi = 327,
        TOKEN_rand = 328,
        TOKEN_randFixed = 329,
        TOKEN_id = 330,
        TOKEN_randNormal = 331,
        TOKEN_randNormalFixed = 332,
        TOKEN_position = 333,
        TOKEN_deltaT = 334,
        TOKEN_time = 335,
        TOKEN_outputTime = 336,
        TOKEN_pow = 337,
        TOKEN_log = 338,
        TOKEN_exp = 339,
        TOKEN_mag = 340,
        TOKEN_magSqr = 341,
        TOKEN_sin = 342,
        TOKEN_cos = 343,
        TOKEN_tan = 344,
        TOKEN_min = 345,
        TOKEN_max = 346,
        TOKEN_minPosition = 347,
        TOKEN_maxPosition = 348,
        TOKEN_average = 349,
        TOKEN_sum = 350,
        TOKEN_sqr = 351,
        TOKEN_sqrt = 352,
        TOKEN_log10 = 353,
        TOKEN_asin = 354,
        TOKEN_acos = 355,
        TOKEN_atan = 356,
        TOKEN_atan2 = 357,
        TOKEN_sinh = 358,
        TOKEN_cosh = 359,
        TOKEN_tanh = 360,
        TOKEN_asinh = 361,
        TOKEN_acosh = 362,
        TOKEN_atanh = 363,
        TOKEN_erf = 364,
        TOKEN_erfc = 365,
        TOKEN_lgamma = 366,
        TOKEN_besselJ0 = 367,
        TOKEN_besselJ1 = 368,
        TOKEN_besselY0 = 369,
        TOKEN_besselY1 = 370,
        TOKEN_sign = 371,
        TOKEN_pos = 372,
        TOKEN_neg = 373,
        TOKEN_transpose = 374,
        TOKEN_diag = 375,
        TOKEN_tr = 376,
        TOKEN_dev = 377,
        TOKEN_symm = 378,
        TOKEN_skew = 379,
        TOKEN_det = 380,
        TOKEN_cof = 381,
        TOKEN_inv = 382,
        TOKEN_sph = 383,
        TOKEN_twoSymm = 384,
        TOKEN_dev2 = 385,
        TOKEN_eigenValues = 386,
        TOKEN_eigenVectors = 387,
        TOKEN_cpu = 388,
        TOKEN_weight = 389,
        TOKEN_set = 390,
        TOKEN_zone = 391,
        TOKEN_OR = 392,
        TOKEN_AND = 393,
        TOKEN_EQ = 394,
        TOKEN_NEQ = 395,
        TOKEN_LEQ = 396,
        TOKEN_GEQ = 397,
        TOKEN_NEG = 398,
        TOKEN_NOT = 399,
        TOKEN_HODGE = 400
      };
    };

    /// (External) token type, as returned by yylex.
    typedef token::yytokentype token_type;

    /// Symbol type: an internal symbol number.
    typedef int symbol_number_type;

    /// The symbol type number to denote an empty symbol.
    enum { empty_symbol = -2 };

    /// Internal symbol number for tokens (subsumed by symbol_number_type).
    typedef unsigned char token_number_type;

    /// A complete symbol.
    ///
    /// Expects its Base type to provide access to the symbol type
    /// via type_get().
    ///
    /// Provide access to semantic value and location.
    template <typename Base>
    struct basic_symbol : Base
    {
      /// Alias to Base.
      typedef Base super_type;

      /// Default constructor.
      basic_symbol ();

      /// Copy constructor.
      basic_symbol (const basic_symbol& other);

      /// Constructor for valueless symbols.
      basic_symbol (typename Base::kind_type t,
                    const location_type& l);

      /// Constructor for symbols with semantic value.
      basic_symbol (typename Base::kind_type t,
                    const semantic_type& v,
                    const location_type& l);

      /// Destroy the symbol.
      ~basic_symbol ();

      /// Destroy contents, and record that is empty.
      void clear ();

      /// Whether empty.
      bool empty () const;

      /// Destructive move, \a s is emptied into this.
      void move (basic_symbol& s);

      /// The semantic value.
      semantic_type value;

      /// The location.
      location_type location;

    private:
      /// Assignment operator.
      basic_symbol& operator= (const basic_symbol& other);
    };

    /// Type access provider for token (enum) based symbols.
    struct by_type
    {
      /// Default constructor.
      by_type ();

      /// Copy constructor.
      by_type (const by_type& other);

      /// The symbol type as needed by the constructor.
      typedef token_type kind_type;

      /// Constructor from (external) token numbers.
      by_type (kind_type t);

      /// Record that this symbol is empty.
      void clear ();

      /// Steal the symbol type from \a that.
      void move (by_type& that);

      /// The (internal) type number (corresponding to \a type).
      /// \a empty when empty.
      symbol_number_type type_get () const;

      /// The token.
      token_type token () const;

      /// The symbol type.
      /// \a empty_symbol when empty.
      /// An int, not token_number_type, to be able to store empty_symbol.
      int type;
    };

    /// "External" symbols: returned by the scanner.
    typedef basic_symbol<by_type> symbol_type;


    /// Build a parser object.
    CloudValueExpressionParser (void * scanner_yyarg, CloudValueExpressionDriver& driver_yyarg, int start_token_yyarg, int numberOfFunctionChars_yyarg);
    virtual ~CloudValueExpressionParser ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

#if PARSERCLOUDDEBUG
    /// The current debugging stream.
    std::ostream& debug_stream () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);
#endif

    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);

    /// Report a syntax error.
    void error (const syntax_error& err);

  private:
    /// This class is not copyable.
    CloudValueExpressionParser (const CloudValueExpressionParser&);
    CloudValueExpressionParser& operator= (const CloudValueExpressionParser&);

    /// State numbers.
    typedef int state_type;

    /// Generate an error message.
    /// \param yystate   the state where the error occurred.
    /// \param yyla      the lookahead token.
    virtual std::string yysyntax_error_ (state_type yystate,
                                         const symbol_type& yyla) const;

    /// Compute post-reduction state.
    /// \param yystate   the current state
    /// \param yysym     the nonterminal to push on the stack
    state_type yy_lr_goto_state_ (state_type yystate, int yysym);

    /// Whether the given \c yypact_ value indicates a defaulted state.
    /// \param yyvalue   the value to check
    static bool yy_pact_value_is_default_ (int yyvalue);

    /// Whether the given \c yytable_ value indicates a syntax error.
    /// \param yyvalue   the value to check
    static bool yy_table_value_is_error_ (int yyvalue);

    static const short int yypact_ninf_;
    static const signed char yytable_ninf_;

    /// Convert a scanner token number \a t to a symbol number.
    static token_number_type yytranslate_ (int t);

    // Tables.
  // YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
  // STATE-NUM.
  static const short int yypact_[];

  // YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
  // Performed when YYTABLE does not specify something else to do.  Zero
  // means the default is an error.
  static const unsigned short int yydefact_[];

  // YYPGOTO[NTERM-NUM].
  static const short int yypgoto_[];

  // YYDEFGOTO[NTERM-NUM].
  static const short int yydefgoto_[];

  // YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
  // positive, shift that token.  If negative, reduce the rule whose
  // number is the opposite.  If YYTABLE_NINF, syntax error.
  static const unsigned short int yytable_[];

  static const short int yycheck_[];

  // YYSTOS[STATE-NUM] -- The (internal number of the) accessing
  // symbol of state STATE-NUM.
  static const unsigned char yystos_[];

  // YYR1[YYN] -- Symbol number of symbol that rule YYN derives.
  static const unsigned char yyr1_[];

  // YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.
  static const unsigned char yyr2_[];


    /// Convert the symbol name \a n to a form suitable for a diagnostic.
    static std::string yytnamerr_ (const char *n);


    /// For a symbol, its name in clear.
    static const char* const yytname_[];
#if PARSERCLOUDDEBUG
  // YYRLINE[YYN] -- Source line where rule number YYN was defined.
  static const unsigned short int yyrline_[];
    /// Report on the debug stream that the rule \a r is going to be reduced.
    virtual void yy_reduce_print_ (int r);
    /// Print the state stack on the debug stream.
    virtual void yystack_print_ ();

    // Debugging.
    int yydebug_;
    std::ostream* yycdebug_;

    /// \brief Display a symbol type, value and location.
    /// \param yyo    The output stream.
    /// \param yysym  The symbol.
    template <typename Base>
    void yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const;
#endif

    /// \brief Reclaim the memory associated to a symbol.
    /// \param yymsg     Why this token is reclaimed.
    ///                  If null, print nothing.
    /// \param yysym     The symbol.
    template <typename Base>
    void yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const;

  private:
    /// Type access provider for state based symbols.
    struct by_state
    {
      /// Default constructor.
      by_state ();

      /// The symbol type as needed by the constructor.
      typedef state_type kind_type;

      /// Constructor.
      by_state (kind_type s);

      /// Copy constructor.
      by_state (const by_state& other);

      /// Record that this symbol is empty.
      void clear ();

      /// Steal the symbol type from \a that.
      void move (by_state& that);

      /// The (internal) type number (corresponding to \a state).
      /// \a empty_symbol when empty.
      symbol_number_type type_get () const;

      /// The state number used to denote an empty symbol.
      enum { empty_state = -1 };

      /// The state.
      /// \a empty when empty.
      state_type state;
    };

    /// "Internal" symbol: element of the stack.
    struct stack_symbol_type : basic_symbol<by_state>
    {
      /// Superclass.
      typedef basic_symbol<by_state> super_type;
      /// Construct an empty symbol.
      stack_symbol_type ();
      /// Steal the contents from \a sym to build this.
      stack_symbol_type (state_type s, symbol_type& sym);
      /// Assignment, needed by push_back.
      stack_symbol_type& operator= (const stack_symbol_type& that);
    };

    /// Stack type.
    typedef stack<stack_symbol_type> stack_type;

    /// The stack.
    stack_type yystack_;

    /// Push a new state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the symbol
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, stack_symbol_type& s);

    /// Push a new look ahead token on the state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the state
    /// \param sym  the symbol (for its value and location).
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, state_type s, symbol_type& sym);

    /// Pop \a n symbols the three stacks.
    void yypop_ (unsigned int n = 1);

    /// Constants.
    enum
    {
      yyeof_ = 0,
      yylast_ = 2825,     ///< Last index in yytable_.
      yynnts_ = 26,  ///< Number of nonterminal symbols.
      yyfinal_ = 149, ///< Termination state number.
      yyterror_ = 1,
      yyerrcode_ = 256,
      yyntokens_ = 162  ///< Number of tokens.
    };


    // User arguments.
    void * scanner;
    CloudValueExpressionDriver& driver;
    int start_token;
    int numberOfFunctionChars;
  };



} // parserCloud
#line 632 "CloudValueExpressionParser.tab.hh" // lalr1.cc:377




#endif // !YY_PARSERCLOUD_CLOUDVALUEEXPRESSIONPARSER_TAB_HH_INCLUDED
